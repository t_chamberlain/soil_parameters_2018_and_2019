# README #

This repository contains all the scripts used to collate, group and parameterise soils for HowLeaky modelling of GBR grains and banana cropping areas. The steps are documented in the following report:

Chamberlain T, Silburn DM, Biggs A, Rattray D and Ellis R 2020, Development of a soil parameter framework to support paddock-scale modelling in the Great Barrier Reef catchments, Paddock Modelling for Grains and Bananas, Technical Report for Reef Report Cards 2018-2019. Queensland Department of Natural Resources, Mines and Energy.

Each script corresponds to a chapter in the report.

Four extra scripts are included which were used for small jobs supporting the modelling for the 2019 Report Card - checking climate files in the Fitzroy, comparing crop footprints in the Burdekin, and analysing soils in grains investment areas in Three Moon Ck in the North Burnett.